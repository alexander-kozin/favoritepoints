# README #

Application for adding and displaing favorite points on the map.

### Features ###
* Hold finger about 2 seconds at some location on the map for adding new point to favorites.
* Display all added points by touching button "My points" at the navigation bar on the screen.
* Move the map to current location by touching on icon on the bottom right corner of the screen.
